const reservationResolver = {
    Query: {
        reservationByUsername: async (_, {username}, {dataSources, usernameToken}) => {
            if(username == usernameToken){
                return await dataSources.accountAPI.reservationByUsername(username);
            }else{
                return null
            }
        }
    },
    Mutation: {
        createReservation: async (_, {reservation}, {dataSources}) => {
            return await dataSources.accountAPI.createReservation(reservation);
        }
    }
}

module.exports = reservationResolver;