const productResolver = {
    Query: {
        productByUsername: async (_, {product}, {dataSources}) => {
        
            return await dataSources.accountAPI.getPurchaseProduct(product);
          
        }
    }
   
}

module.exports = productResolver;