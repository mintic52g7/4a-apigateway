const gimnasioResolver = {
    Query: {
        gimnasioByVariable: async (_, {id}, {dataSources}) => {
        
            return await dataSources.authAPI.getGimnasio(id);
          
        }
    }
   
}

module.exports = gimnasioResolver;