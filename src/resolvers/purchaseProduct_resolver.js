const purchaseProductResolver = {
    Query: {
         purchaseProduct: async (_, {id}, {dataSources, usernameToken}) => {

                 return await dataSources.accountAPI.getPurchaseProduct(id);
             }
         },
    Mutation: {
        createPurchaseProduct: async (_, {id}, {dataSources}) => {
             return await dataSources.accountAPI.postPurchaseProduct(id);
        }
    }, 
    Mutation: {
        deletePurchaseProduct: async (_, {id}, {dataSources}) => {
             return await dataSources.accountAPI.deletePurchaseProduct(id);
        }
    }
}


module.exports = purchaseProductResolver;