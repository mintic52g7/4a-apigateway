const accountResolver = require('./account_resolvers');
const authResolver = require('./auth_resolvers');
const productResolver = require('./product_resolver');
const purchaseProductResolver = require('./purchaseProduct_resolver');
const reservationResolver = require('./reservation_resolver');
const gimnasioResolver = require(`./gimnasio_resolver`);

const lodash = require('lodash');

const resolvers = lodash.merge(accountResolver, authResolver, productResolver,purchaseProductResolver,reservationResolver, gimnasioResolver);

module.exports = resolvers;