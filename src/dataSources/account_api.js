const serverConfig = require('../server');
const { RESTDataSource } = require('apollo-datasource-rest');

class AccountAPI extends RESTDataSource{

    constructor(){
        super();
        this.baseURL = serverConfig.account_api_url;
    }

    async accountByUsernameRequest(username){
        return await this.get(`/accounts/${username}`);
    }

    async createAccount(accountData){
        return await this.post('/accounts/', accountData);
    }

    async createReservation(reservation){
        return await this.post('/reservation/', reservation);
    }

    async reservationByUsername(username){
        return await this.get(`/reservation/${username}`)
    }
    async getPurchaseProduct(id){
        return await this.get(`/purchase/${id}`)
    }
    async postPurchaseProduct(id){
        return await this.post(`/purchase/`, id);
    }

    async deletePurchaseProduct(id){
        return await this.delete(`/purchase/${id}`);
    }

}

module.exports = AccountAPI;