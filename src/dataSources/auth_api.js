const serverConfig = require('../server');
const { RESTDataSource } = require('apollo-datasource-rest');

class AuthAPI extends RESTDataSource{
    constructor(){
        super();
        this.baseURL = serverConfig.auth_api_url;
    }

    async loginRequest(credentials){
        return await this.post('/rest-auth/login/', credentials);
    }

    async createUser(userData){
        return await this.post('/rest-auth/registration/', userData);
    }

    async getGimnasio(id){
        return await this.get(`/gimnasio/sedes/${id}`);
    }
}

module.exports = AuthAPI;