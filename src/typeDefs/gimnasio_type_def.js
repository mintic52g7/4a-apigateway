const { gql } = require('apollo-server');

const gimnasioTypeDefs = gql` 

    type getGimnasio{
        nombre: String!
        direccion: String!
        telefono: Int 
    }

    type getGimnasioInput{
        nombre: String!
        direccion: String!
        telefono: Int 
    }

    type Query{
        gimnasioByVariable(id: String!): getGimnasio
        mygimnasio: getGimnasio
    }
`;

module.exports = gimnasioTypeDefs;


