const accountTypeDefs = require('./account_type_def');
const authTypeDefs = require('./auth_type_def');
const productTypeDefs = require('./product_type_def');
const reservationTypeDefs = require('./reservation_type_def');
const purchaseProductTypeDefs = require('./purchaseProduct_type_def');
const gimnasioTypeDefs = require('./gimnasio_type_def');

const schemasArray = [accountTypeDefs, authTypeDefs, reservationTypeDefs,productTypeDefs,purchaseProductTypeDefs, gimnasioTypeDefs];

module.exports = schemasArray;