const { gql } = require('apollo-server');

const productTypeDefs = gql`
    type Product{
        
        productName: String!
        price: Float!
        unit: Int
    }

    input ProductInput{
        productName: String!
        price: Float!
        value: Int
    }

     type Query {
        productByUsername(productName: String!): Product
    }

     type Mutation{
        createProduct(product: ProductInput!): Product
    }
`;

module.exports = productTypeDefs;