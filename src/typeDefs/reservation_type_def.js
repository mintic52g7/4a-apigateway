const { gql } = require('apollo-server');

const reservationTypeDefs = gql`
    type Reservation{
        id: String!
        username: String!
        reservationDate: String!
        club: String!
        
    }

    input ReservationInput{
        username: String!
        reservationDate: String!
        club:String!
    }

    extend type Query {
        reservationByUsername(username: String!): [Reservation]
    }

    extend type Mutation{
        createReservation(reservation: ReservationInput!): Reservation
    }
`;

module.exports = reservationTypeDefs;