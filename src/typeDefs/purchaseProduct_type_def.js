const { gql } = require('apollo-server');

const purchaseProductTypeDefs = gql`
    type PurchaseProduct{
        id: String!
        username: String!
        product:String!
        productValue:Int!
        purchaseDate: String!
        
        
    }

    input PurchaseProductInput{
        id: String!
        username: String!
        product:String!
        productValue:Int!
        purchaseDate: String!
    }

    type DeleteProduct{
        id: String
    }

   type Query {
        purchaseProduct(id: String!): PurchaseProduct
        myProduct: PurchaseProduct
    }

    type Mutation{
        createPurchaseProduct(id: PurchaseProductInput!): PurchaseProduct
    }

    type Mutation{
        deletePurchaseProduct(id: String!): DeleteProduct 
    }
`;

module.exports = purchaseProductTypeDefs;