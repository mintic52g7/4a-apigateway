const { gql } = require('apollo-server');

const accountTypeDefs = gql` 
    type Account{
        username: String!
        reservationDate: String!
        club: String!
    }

    type Query{
        accountByUsername(username: String!): Account
        myAccount: Account
    }
`;

module.exports = accountTypeDefs;